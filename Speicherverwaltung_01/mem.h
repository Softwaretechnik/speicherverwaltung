/* ------------------------------------------------------------------------- */
/* Datei:              mem.h                                                 */
/* Autor:              Hannes Range, Marcus Meiburg                          */
/* Version:            1.5                                                   */
/* letzte Bearbeitung: 31.05.2013                                            */
/* Beschreibung:       Headerdatei der doppelt verk. Liste "mem.c"           */
/* ------------------------------------------------------------------------- */

#ifndef _MEM_
#define _MEM_

/* Includes */
#include <stdio.h>
#include <stdlib.h>

typedef enum {FALSE=0, TRUE} Boolean;
typedef enum {LUECKE=0, PROZESS} Type;

// Size of the physical memory
#define MEMORY_SIZE 100

/* ------------------------------------------------------------------------- */
/*          Repraesentation einer Zelle der doppelt verketteten Liste        */
/* ------------------------------------------------------------------------- */
struct mem_zelle {
  unsigned pid;        // Prozess ID (Einzigartzig vom OS vergeben)
  Type type;          // P - Prozess , L - Luecke (freier Speicherplatz)
  unsigned pos;        // Anfangsposition
  unsigned size;       // Gibt die Groesse des Prozesses/Luecke an
  struct mem_zelle* next; // N�chste Zelle
  struct mem_zelle* prev; // Vorherige Zelle
};
typedef struct mem_zelle mZelle; 

/* ------------------------------------------------------------------------- */
/*                         Prototypenvereinbarung                            */
/* ------------------------------------------------------------------------- */
Boolean initMemory();
/* Fuegt einen Prozess zur Speicherverwaltung hinzu */
Boolean addProcess(unsigned pid, unsigned size);
/* Loescht einen Prozess aus der Speicherverwaltung */
Boolean remProcess(unsigned pid);
/* Verschmilzt benachtbarte Luecken miteinander */
void merge(mZelle *akt_zelle);
/* Partitioniert die Speicherverwaltung*/
void kompaktierung();
void print();

#endif