/* ------------------------------------------------------------------------- */
/* Datei:              mem.cpp                                               */
/* Autor:              Hannes Range, Marcus Meiburg                          */
/* Version:            1.6                                                   */
/* letzte Bearbeitung: 01.06.2013                                            */
/* Beschreibung:       Implementation einer Verketteten Liste mit            */
/*                     Kompaktierung. Einfuegen mit First Fit Methode        */
/* ------------------------------------------------------------------------- */

#include "stdafx.h"
#include "mem.h"

/* ------------------------------------------------------------------------- */
/*                       Globale Variablen                                   */
/* ------------------------------------------------------------------------- */
/* Anfang der verketteten Liste*/
mZelle* mAnfang = NULL;

/* ------------------------------------------------------------------------- */
/*                       Initialiesierung                                    */
/* ------------------------------------------------------------------------- */
Boolean initMemory() {
  if(mAnfang == NULL) {
    if((mAnfang =(mZelle*)malloc(sizeof(mZelle)))!= NULL) {
      mAnfang->next = NULL;
      mAnfang->prev = NULL;
      /* Zelleninhalt bestimmen */
      mAnfang->type = LUECKE;
      mAnfang->pos = 0;
      mAnfang->size = MEMORY_SIZE;
      mAnfang->pid = 0;
      return TRUE;
    }
  }
  return FALSE;
}

/* ------------------------------------------------------------------------- */
/*                   Loeschen eines Prozesses                                */
/* ------------------------------------------------------------------------- */
Boolean remProcess(unsigned pid) {
  mZelle *akt_zelle = NULL;
  akt_zelle = mAnfang;
  /* Verkettete Liste durchgehen bis Prozess gefunden od. am Ende angekommen */
  while(akt_zelle->next != NULL && akt_zelle->pid != pid) {
    akt_zelle = akt_zelle->next; 
  }
  /* Wurde der Prozess gefunden */
  if(akt_zelle->pid == pid) {
    /* Prozess wird zur Luecke */
    akt_zelle ->type = LUECKE;
    akt_zelle->pid = 0;
    /* Verschmelzung durchfuehren */
    merge(akt_zelle);
    return TRUE;
  }
  return FALSE;
}

/* ------------------------------------------------------------------------- */
/*                          Verschmelzen                                     */
/* ------------------------------------------------------------------------- */
void merge(mZelle *akt_zelle) {
  mZelle *vorgaenger = NULL, *nachfolger = NULL;

  vorgaenger = akt_zelle->prev;
  nachfolger = akt_zelle->next;

  /* Kompaktierung zur rechten Seite */
  /* Pruefe ob es einen Nachfolger gibt */
  if(nachfolger != NULL) {
    /* Ist der VOrgaenger eine Luecke */
    if(nachfolger->type == LUECKE) {
      /* Anpassen der Zellen Informationen */
      akt_zelle->size = akt_zelle->size + nachfolger->size;
      /* Hat der Nachfolger noch einen Nachfolger */
      if(nachfolger->next != NULL) {
          /* Nachfolger des Nachfolgers mit der aktuellen Zelle verknuepfen */
          nachfolger->next->prev = akt_zelle;
          /* aktuelle Zelle auf den neuen Nachfolger zeigen lassen */
          akt_zelle->next = nachfolger->next;
      } else {
          akt_zelle->next = NULL;
      }
      /* Speicherzelle der Luecke freigeben */
      free(nachfolger);
    }
  }

  /* Kompaktierung zur linken Seite    */
  /* Pruefe ob es einen Vorgaenger gibt */
  if(vorgaenger != NULL) {
    /* Ist der Vorgaenger eine Luecke */
    if(vorgaenger->type == LUECKE) {
      /* Anpassen der Zellen Informationen */
      akt_zelle->pos = vorgaenger->pos;
      akt_zelle->size = akt_zelle->size + vorgaenger->size;
      /* Hat der Vorgaenger noch weitere Vorgaenger */
      if(vorgaenger->prev != NULL) {
          /* Vorgaenger des Vorgaengers mit der aktuellen Zelle verknuepfen */
          vorgaenger->prev->next = akt_zelle;
          /* aktuelle Zelle auf den neuen Vorgaenger zeigen lassen */
          akt_zelle->prev = vorgaenger->prev;
      /* Aktuelle Zelle wird neuer Anfang */
      } else {
          akt_zelle->prev = NULL;
          mAnfang = akt_zelle;
      }
      /* Speicherzelle der Luecke freigeben */
      free(vorgaenger);
    }
  }
}

/* ------------------------------------------------------------------------- */
/*                  Hinzufuegen eines Prozesses                              */
/* ------------------------------------------------------------------------- */
Boolean addProcess(unsigned pid, unsigned size) {

  mZelle *zeiger = NULL, *neuZelle = NULL, *nachfolger = NULL;

  zeiger = mAnfang;
  /* Erste Luecke mit genug freiem Speicher suchen (FirstFit) */
  while(zeiger->next != NULL && !(zeiger->size >= size && zeiger->type == LUECKE)) {
    zeiger = zeiger->next;
  }

  /* Nachfolger der Luecke speichern */
  nachfolger = zeiger->next;

  /* Hat die gefundene Zelle die exakte groesse */
  if(zeiger->size == size) {
    zeiger->pid = pid;
    zeiger->type = PROZESS;
    return TRUE;
  /* Die Zelle ist groesser */
  } else if(zeiger->size > size) {
    /* Neue Zelle nach dem Zeiger einfuegen */
    if((neuZelle = (mZelle*)malloc(sizeof(mZelle))) != NULL) { 
        neuZelle->next = nachfolger;
        neuZelle->prev = zeiger;
        zeiger->next = neuZelle;
        /* Falls es einen Nachfolger gibt, den Vorgaeger anpassen */
        if(nachfolger != NULL) {
          nachfolger->prev = neuZelle;
        }
        zeiger->size = zeiger->size - size;
        /* Zelleninhalt bestimmen */
        neuZelle->pos = zeiger->size;
        neuZelle->size = size;
        neuZelle->pid = pid;
        neuZelle->type = PROZESS;
        return TRUE;
    } else {
        //logGeneric("Kein Speicherplatz mehr vorhanden!");
        return FALSE;
    }
  }
  return FALSE;
}

/* ------------------------------------------------------------------------- */
/*                              Kompaktierung                                */
/* ------------------------------------------------------------------------- */
void kompaktierung() {
  mZelle *zeiger = NULL;     /* Zeiger auf die naechste Luecke */
  mZelle *vorgaenger = NULL; /* Zeiger auf den Vorgaenger der Luecke */
  mZelle *nachfolger = NULL; /* Zeiger auf den Nachfolger der Luecke */
  zeiger = mAnfang;

  /* Nur ausfuehren wenn es mehr als einen Eintrag gibt */
  if(mAnfang->next != NULL) {
    while(zeiger != NULL) {
      nachfolger = zeiger->next;
      vorgaenger = zeiger->prev;

      /* Wenn der Zeiger nicht auf den Anfang zeigt und eine Luecke ist */
      if(zeiger != mAnfang && zeiger->type == LUECKE) {
        if(mAnfang->type != PROZESS) {
            /* Luecke neu verketten */
            mAnfang->next->prev = zeiger;
            zeiger->next = mAnfang->next;
            mAnfang->next = zeiger;
            zeiger->prev = mAnfang;
        /* Luecke mit Anfang verketten (Anfang ist ein Prozess) */
        } else {
            /* Luecke neu verketten */
            mAnfang->prev = zeiger;
            zeiger->next = mAnfang;
            zeiger->prev = NULL;
            /* Zeiger als neuen Anfang setzen */
            mAnfang = zeiger;
            mAnfang->pos = 0;
        }

        /* Verschmelzen des Anfangs */
        if(mAnfang->next != NULL && mAnfang->next->type == LUECKE) {
          merge(mAnfang->next);
        }

        /* Prozess neu verketten */
        if(nachfolger != NULL) {
          nachfolger->prev = vorgaenger;
        }
        vorgaenger->next = nachfolger;

        /* Zeiger auf den Nachfolger der letzen Luecke setzen */
        zeiger = nachfolger;
      }
      /* Wenn der Nachfolger nicht schon das Ende der Liste ist, auf naechste Zelle zeigen */
      if(nachfolger != NULL)
        zeiger = zeiger->next;
    }

    /* Positionen anpassen */
    zeiger = mAnfang->next;
    while(zeiger != NULL) {
      zeiger->pos = zeiger->prev->pos + zeiger->prev->size;
      zeiger = zeiger->next;
    }
  }
}

int main(int argc, char* argv[]) {
  char dummy[81];
  printf("Program started...\n\n");
  if(initMemory()) {
    print();
    printf("### Prozess 1 - 7 hinzufuegen ###\n\n");
    printf("### Prozess 1,3,5,7 loeschen ###\n\n");
    addProcess(1,5);
    addProcess(2,10);
    addProcess(3,20);
    addProcess(4,40);
    addProcess(5,10);
    addProcess(6,5);
    addProcess(7,10);
    remProcess(1);
    remProcess(3);
    remProcess(5);
    remProcess(7);
    print();
    printf("### 1. Kompaktieren ###\n\n");
    kompaktierung();
    print();
    printf("### Prozess 2 loeschen ###\n\n");
    remProcess(2);
    print();
    printf("### 2. Kompaktieren ###\n\n");
    kompaktierung();
    print();
    printf("### Prozess 4 und 6 loeschen ###\n\n");
    remProcess(4);
    remProcess(6);
    print();
    printf("### 3. Kompaktieren ###\n\n");
    kompaktierung();
    print();
    printf("### Prozess hinzufuegen ###\n\n");
    addProcess(1,40);
    addProcess(2,20);
    addProcess(3,40);
    print();
    printf("### Prozess 1 loeschen ###\n\n");
    remProcess(1);
    print();
    printf("### 4. Kompaktieren ###\n\n");
    kompaktierung();
    print();
  }
  printf("...program finished!\n");
  gets(dummy);
  return 0;
}

/* Gibt alle Speicherzellen in einer Tabelle aus*/
void print() {
  mZelle* zeiger = mAnfang;
  printf("| PID |   Type  |   Pos. |   Size |   MyAddress | PrevAddress | NextAddress |\n");
  printf("-----------------------------------------------------------------------------\n");
  while(zeiger != NULL) {
    printf("|%5u| %s | %6d | %6d | %11p | %11p | %11p |\n",zeiger->pid, zeiger->type ? "Prozess" : "Luecke " ,
           zeiger->pos , zeiger->size, zeiger, zeiger->prev,zeiger->next);
    zeiger = zeiger->next;
  }
  printf("-----------------------------------------------------------------------------\n");
}